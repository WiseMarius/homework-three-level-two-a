#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	connect(ui->pushButton,
		SIGNAL(clicked()),
		this,
		SLOT(buttonClicked()));
}

void MainWindow::buttonClicked()
{
	ui->lineEdit_2->setText(ui->lineEdit->text());
}

MainWindow::~MainWindow()
{
    delete ui;
}
